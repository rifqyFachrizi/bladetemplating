@extends('master')
@section('content')
<div class="col">
    <div class="card" style="width: 18rem;">
        <div class="card-body">
          <h1>{{ $data->nama }}</h1>
          <h4>{{ $data->umur }}</h4>
          <p>{{ $data->bio }}</p>
        </div>
    </div>
</div>
@endsection