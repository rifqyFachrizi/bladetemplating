<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use PhpParser\Node\Expr\Cast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[CastController::class, 'index']);
Route::get('/table', [CastController::class, 'index']);
Route::get('/data-tables', function () {
    return view('dataTables',[
        'title' => 'Data Table'
    ]);
});
Route::get('/cast/create', [CastController::class,'create']);
Route::post('/cast', [CastController::class, 'store']);
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{cast:id}', [CastController::class, 'show']);
Route::get('/cast/{cast:id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast:id}', [CastController::class, 'update']);
Route::delete('/cast/{cast:id}', [CastController::class, 'destroy']);