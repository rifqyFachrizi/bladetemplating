<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('post.create',[
            'title' => 'Create'
        ]);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast/create');
    }

    public function index()
    {
        
        $datas = DB::table('casts')
                    ->select('id','nama','umur','bio')
                    ->get();
        // return dd($datas);
        return view('post.index',[
            'title' => 'Data Cast',
            'datas' => $datas
        ]);
    } 

    public function show($id){
        $datas = DB::table('casts')
                    ->select('id','nama','umur','bio')
                    ->get();
        $dataCollect = collect($datas);
        $data = $dataCollect->firstWhere('id', $id);

        return view('show',[
            'title' => 'Show',
            'data' => $data 
        ]);
        
    }

    public function edit($id)
    {
        $datas = DB::table('casts')
                    ->select('id','nama','umur','bio')
                    ->get();
        $dataCollect = collect($datas);
        $data = $dataCollect->firstWhere('id', $id);

        return view('edit',[
            'title' => 'Edit',
            'data' => $data
        ]);
    }


    public function update($id, Request $request){
        $affected = DB::table('casts')
              ->where('id', $id)
              ->update([
                  'nama' => $request['nama'],
                  'umur' => $request['umur'],
                  'bio' => $request['bio']
              ]);

        return redirect('/cast'. '/'. $id .'/edit');
    }

    public function destroy($id){
        DB::table('casts')->where('id', $id)->delete();

        return redirect('/');
    }
}
