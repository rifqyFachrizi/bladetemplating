@extends('master')

@section('content')

<a href="/cast/create" class="btn btn-primary mb-2">Tambah</a>
<table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">#</th>
        <th>Name</th>
        <th style="width: 40px">Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($datas as $d) : ?>
      <tr>
        <td>{{ $d->id }}.</td>
        <td>{{ $d->nama }}</td>
        <td>
          <a href="/cast/{{ $d->id }}" class="btn btn-warning" >detail</a>
          <a href="/cast/{{ $d->id }}/edit" class="btn btn-primary" >edit</a>
          <form action="/cast/{{$d->id}}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-danger my-1" value="Delete">
        </form>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
@endsection